# SocketIO4Mongo

Example of WebSocket with the integration of socket.io (Node.JS) with MongoDB through a simple WebChat project.

## Instructions

Access the project folder and execute the commands below in terminal.

Run the server (terminal):
```
node server
```

Run the client side (terminal):
```
node startclient
```

## References
[Basic tutorial](http://rcdevlabs.github.io/2015/02/11/criando-um-server-de-push-notifications-para-notificacoes-em-tempo-real-com-socket-io-e-nodejs/) *in portuguese*.

Chat developed based on [MongoChat Tutorial - Part 2](https://www.youtube.com/watch?v=8Y6mWhcdSUM) and [MongoChat Tutorial - Part 1](https://www.youtube.com/watch?v=hrRue5Rt6Is).

[More about WebSockets](https://developer.mozilla.org/en-US/docs/Web/API/WebSockets_API)