'use strict';

let mongoose  = require('mongoose');
let debug     = require('debug')('connection:mongoose');
let env       = require('./env');

mongoose.connect(env.database.uri, { useNewUrlParser: true })

mongoose.set('debug', true);
mongoose.Promise = global.Promise;
const db = mongoose.connection;

db.on('error', function(err) {
  debug(err);
  console.log(`Erro`);
});

db.on('openUri', () => {
  console.log(`Connected!`);
});

db.once('openUri', function (callback) {
  debug('Connected to mongodb');
  console.log(`Connected!`);
});

module.exports = mongoose;