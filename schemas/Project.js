'use strict';

let mongoose = require('../configs/MongooseConfig');
let ProjectSchema = new mongoose.Schema({  
    projectname    : {type: String},
    created_at     : {type: Date},
    started_at     : {type: Date},
    finished_at    : {type: Date},
    notes          : {type: String},
    is_deleted     : {type: Boolean},
    user_cpf       : {type: String},
    numqm          : {type: String},
    ovens          : {},
    pieces         : {}
});
module.exports = mongoose.model('projects', ProjectSchema);
  

