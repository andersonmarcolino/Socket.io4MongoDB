'use strict';

//Run with node server

const env = require('./configs/env');
const http = require('http');
const express = require('express');
const mongoose  = require('mongoose');
const bodyParser = require('body-parser');
const app = express();

//Porta 4555 para o server http do socket.io
var server = http.createServer(app).listen(4555);
var io = require('socket.io').listen(server);

app.unsubscribe(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
const router = express.Router();

//Connection with MongoDB using Mongoose
mongoose.connect(env.database.uri, { useNewUrlParser: true }, (err, db) => {
    if(err){
        console.log('Error - Can not connected to mongo: ' + err);
    }
    
    console.log('Connected to ' + env.database.uri);
    //db.close();
    let projects = db.collection('chats');

    io.on('connection', (socket) => {
        
        //Function to send MongoDB status for client
        function sendStatus(s){
            socket.emit('status', s);
        }

        //Get projects
        projects.find().limit(10).sort({started_at:1}).toArray((err, res) => {
            if(err){
                throw err;
            }
            //Emit the project
            socket.emit('output', res);
        });

        //Handle input events
        socket.on('input',(data) => {
            let name =  data.name;
            let message = data.message;
            //Chek data
            if(name == '' || message == ''){
                //Send error status
                sendStatus('Please enter the correct data!');
            }else{
                //Insert message 
                projects.insert({name : name, message: message}, () => {
                    io.emit('output', [data]);

                    //Send status object
                    sendStatus({
                        message: "Message sent",
                        clear: true
                    });
                });
            };
        });

        //Handle Clear
        socket.on('clear', ()=> {
            //Remove all data from collection
            projects.remove({}, ()=>{
                socket.emit('cleared');
            })

        })
    });


    
})

//Socket.io
let emission = function(req, res, next){
    let notify = req.query.notification || '';
    if(notify != ''){
        io.emit('notification', notify);
        next();
    }else{
        next();
    }
}
app.use(emission);
app.use('/api', router);
router.route('/notificar').get(function(req, res){
    res.json({message: "Teste de rota"})
})
app.listen(env.server.port);
console.log('Connect to port ' + env.server.port);