'use strict';

//Run with node startclient

const env = require('./configs/env');
const http = require('http');
const server = http.createServer(servidor);
const fs = require('fs');
function servidor(requisicao, resposta){
    resposta.writeHead(200);
    resposta.end(fs.readFileSync('pages/client.html'));
};

server.listen(env.client.port, function(){
    console.log('HTML Server on port ' + env.client.port);
});
